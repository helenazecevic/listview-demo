package eu.execom.hzecevic.listdemo.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import eu.execom.hzecevic.listdemo.R;
import eu.execom.hzecevic.listdemo.model.Task;

public class TasksAdapter extends BaseAdapter {

    private List<Task> tasks = new ArrayList<>();

    private final LayoutInflater layoutInflater;

    private final Context context;

    public TasksAdapter(Context context, List<Task> tasks) {
        this.tasks = tasks;
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return tasks.size();
    }

    @Override
    public Object getItem(int position) {
        return tasks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;

        if (convertView == null) {
            // create a new view holder since we don't have a view to reuse
            viewHolder = new ViewHolder();

            // inflate view item
            convertView = layoutInflater.inflate(R.layout.view_item_task, parent, false);

            // store the views in a view holder for later reuse
            viewHolder.done = (CheckBox) convertView.findViewById(R.id.done);
            viewHolder.text = (TextView) convertView.findViewById(R.id.text);

            // save view holder for later reuse
            convertView.setTag(viewHolder);
        } else {
            // we already have a view holder
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // get item
        final Task task = (Task) getItem(position);

        // populate view item with task's data
        viewHolder.done.setChecked(task.isDone());
        viewHolder.text.setText(task.getText());

        viewHolder.done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Task currentTask = tasks.get(position);

                // save the current state to task, needed because of cell reuse
                currentTask.setDone(viewHolder.done.isChecked());
                tasks.set(position, currentTask);

            }
        });

        return convertView;
    }

    private static class ViewHolder {

        private CheckBox done;

        private TextView text;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }
}
