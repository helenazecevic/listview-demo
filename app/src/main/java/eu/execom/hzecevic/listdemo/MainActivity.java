package eu.execom.hzecevic.listdemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import eu.execom.hzecevic.listdemo.adapter.TasksAdapter;
import eu.execom.hzecevic.listdemo.model.Task;

public class MainActivity extends AppCompatActivity {

    private ListView listView;

    private TasksAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    /**
     * Initialize list view
     */
    private void init() {
        // get listView's reference
        listView = (ListView) findViewById(R.id.listView);

        // initialize items
        final List<Task> taskList = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            taskList.add(new Task("A very cool task " + i, i % 2 == 0));
        }

        // initialize adapter
        adapter = new TasksAdapter(this, taskList);

        // set adapter to list
        listView.setAdapter(adapter);
    }
}
